package com.aykay.jserver;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * @author aykay
 */
import com.aykay.ctequerybuilder.DataExportInfo;
import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.List;
import java.sql.Connection;
import de.schlichtherle.truezip.file.TVFS;
import de.schlichtherle.truezip.fs.FsSyncException;


public class EventExporter {

    private DataSource dataSource;
    public  EventExporter(DataSource dataSource)
    {
        this.dataSource = dataSource;
    }

    public void export(List<DataExportInfo> exportInfo) throws SQLException, FsSyncException {

        for (DataExportInfo i : exportInfo)
        {
            exportEvents(i);
        }
        TVFS.umount();
    }

    private void exportEvents(DataExportInfo exportInfo) throws SQLException {
        final Connection conn = dataSource.getConnection();
        Statement stmt = conn.createStatement();

        try
        {
            loadEvents(exportInfo.getResourceId(), stmt);
            exportEvents(stmt);
            markEventsAsExported(stmt);
        }
        finally
        {
            try
            {
                stmt.close();
                conn.close();
            }
            catch (SQLException ex) {
                System.out.println(ex.toString());
            }
        }
    }
    private  void loadEvents(String ressNr, Statement stmt) throws SQLException
    {
        String query ="SELECT PROJNR,PINDEX,NKNDATUM INTO #EINSAETZE_ZU_VERSENDEN_TEMP FROM PLANUNG WHERE STATUSSEND =1 AND RESSNR ='" + ressNr +"'";
        stmt.executeUpdate(query);
    }
    public void exportEvents(Statement stmt) throws SQLException {

       /* EventsQueryLoader loader = new EventsQueryLoader();
        QueryBuilder p = new QueryBuilder();
        for (TableDescriptor tableDescriptor : loader.getTablesToExport())
        {
            String query = p.getQueryForTable(tableDescriptor, loader.getQueryParts());
            ResultSet einsaetzeResult = stmt.executeQuery(query);
            try {
                new ResultSetToXmlExporter().exportToXml(einsaetzeResult, tableDescriptor.getTableName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }*/
    }

    private void markEventsAsExported(Statement stmt) throws SQLException {
        stmt.executeUpdate("UPDATE PLANUNG SET STATUSSEND =2 FROM PLANUNG p INNER JOIN #EINSAETZE_ZU_VERSENDEN_TEMP pt ON p.PROJNR = pt.PROJNR AND p.PINDEX = pt.PINDEX WHERE p.NKNDATUM = pt.NKNDATUM OR p.NKNDATUM is NULL");
    }

}
