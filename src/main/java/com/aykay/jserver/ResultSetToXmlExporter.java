package com.aykay.jserver;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author aykay
 */
import de.schlichtherle.truezip.file.TFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import de.schlichtherle.truezip.file.TFileWriter;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class ResultSetToXmlExporter {

    private  Boolean finished = false;
    public void exportToXml(ResultSet resultSet , String tableName) throws FileNotFoundException {

        finished = false;
        int counter = 0;

        while (!finished)
        {
            TFile entry = new TFile("F:/archive/archive.zip/dir/" + tableName + "_" + counter++ + ".xml");

            try(TFileWriter writer = new TFileWriter(entry);)
            {
                writer.write("<DATAPACKET Version=\"2.0\">");
                ResultSetMetaData metaData = resultSet.getMetaData();
                writeMetadata(metaData, writer);
                writeData(resultSet, metaData, writer);
                writer.write("</DATAPACKET>");
            }
            catch(Exception ex)
            {}
        }
    }

    private void writeMetadata( ResultSetMetaData metaData,  TFileWriter writer) throws SQLException, IOException {

        writer.write("<FIELDS>");
        for(int i =0; i<metaData.getColumnCount();i++){
            String columnName = metaData.getColumnName(i);
            int columnType = metaData.getColumnType(i);
            writer.write("<FIELD> attrname=\"");
            writer.write(columnName);
            writer.write("\" fieldtype =\"");
            writer.write(columnType);
            writer.write("\" </FIELD>");
        }
        writer.write("</FIELDS>");
    }

    private void writeData(ResultSet resultSet, ResultSetMetaData metaData,  TFileWriter writer) throws IOException, SQLException {
        int counter = 0;
        writer.write("<ROWDATA>");
        while (counter <= 1000 && !finished)
        {
            counter++;
            if (resultSet.next()) {
                writer.write("<ROW ");
                    for(int i =0; i<metaData.getColumnCount();i++){
                        String columnName = metaData.getColumnName(i);
                        String columnValue = resultSet.getString(i);
                        writer.write(columnName);
                        writer.write("=\"");
                        writer.write(columnValue);
                        writer.write("\"");

                }
                writer.write("></ROW>");
            }
            else {
                finished = true;
            }
        }
        writer.write("</ROWDATA>");
    }
}

