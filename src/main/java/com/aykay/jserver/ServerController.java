package com.aykay.jserver;

import de.schlichtherle.truezip.file.TVFS;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import org.apache.commons.dbcp.BasicDataSource;

import javax.sql.DataSource;
import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class ServerController implements Initializable {

    @FXML
    private ComboBox<String> availableTable;

    @FXML
    private Button executeBtn;

    @FXML
    private Button previousPage;

    @FXML
    private ListView<String> names;

    @FXML
    void execute(ActionEvent event) throws Exception {

    }

    @FXML
    void previousPageAction(ActionEvent event
    ) {
    }

    @FXML
    private void handleComboAction(ActionEvent event
    ) {

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        assert (names != null);
        names.getItems().addAll("12345", "6789", "99887744");

    }



}
